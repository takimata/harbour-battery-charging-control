#ifndef CHARGECONTROLLER_HPP
#define CHARGECONTROLLER_HPP

#include <optional>
#include <string>
#include <string_view>
#include <utility>

struct LRC {
  // sysfs paths for limited range charging
  std::string_view ENABLE;
  std::string_view SOCMAX;
};

class ChargeControllerI {
  public:
  /**
   * @brief Generic ChargeController. Parameters are sysfs paths to the respective files.
   * @param batt_chg_enable
   * @param lrc_enable
   * @param lrc_socmax
   */
  ChargeControllerI(const std::string_view batt_chg_enable, const std::optional<const struct LRC> lrc);

  virtual int toggle_charging(bool enable);
  virtual int limited_range_charge_charging(unsigned int upper_limit);
  virtual int limited_range_charge_disable();
  virtual bool supports_kernel_lrc();

  protected:
  // sysfs paths
  const std::string_view BATT_CHG_ENABLE;
  // if no value present, kernel doesn't support range limited charging
  const std::optional<LRC> lrc;
};

class ChargeControllerXperia_X : public ChargeControllerI {
  public:
  ChargeControllerXperia_X();

  /**
   * @brief matchDevice
   * @return whether this controller is capable of controlling this device
   */
  static bool matchDevice();

};

class ChargeControllerXperia_10_II : public ChargeControllerI {
  public:
  ChargeControllerXperia_10_II();

  /**
   * @brief matchDevice
   * @return whether this controller is capable of controlling this device
   */
  static bool matchDevice();
};

class ChargeControllerXperia_10_Plus : public ChargeControllerI {
  public:
  ChargeControllerXperia_10_Plus();

  /**
   * @brief matchDevice
   * @return whether this controller is capable of controlling this device
   */
  static bool matchDevice();
};

#endif  // CHARGECONTROLLER_HPP
