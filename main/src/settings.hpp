#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <optional>
#include <QObject>
#include <QSettings>

extern const QString SK_Auto;
extern const QString SK_upperLimit;

class Settings : public QObject {
  Q_OBJECT
  Q_PROPERTY(bool automaticMode READ getAutomaticMode WRITE setAutomaticMode NOTIFY automaticModeChanged)
  Q_PROPERTY(uint upperLimit READ getUpperLimit WRITE setUpperLimit NOTIFY upperLimitChanged)

  QSettings settings;

  public:
  explicit Settings(QObject *parent = nullptr);

  bool getAutomaticMode() const;
  void setAutomaticMode(bool isAuto);

  uint getUpperLimit() const;
  void setUpperLimit(uint chargeLimit);

  std::optional<bool> getCachedKernelSupportsLRC() const;
  void setCachedKernelSupportsLRC(bool kernel_supports_lrc);

  signals:
  void automaticModeChanged();
  void upperLimitChanged();

  public slots:
};

#endif  // SETTINGS_HPP
