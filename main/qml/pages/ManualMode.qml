import QtQuick 2.0
import Sailfish.Silica 1.0
import org.freedesktop.contextkit 1.0

import "../panes"

Page {
  id: pageManualMode
  allowedOrientations: Orientation.All

  SilicaFlickable {
    anchors.fill: parent

    PullDownMenu {
      MenuItem {
        text: qsTr("Automatic mode")
        onClicked: {
          cppModel.automaticMode = true
          settings.automaticMode = true
          pageStack.replaceAbove(null, Qt.resolvedUrl("AutomaticMode.qml"),
                                 null, PageStackAction.Immediate)
        }
      }
    }
    PageHeader {
      title: qsTr("Manual charging control")
    }

    Column {
      width: parent.width
      anchors.verticalCenter: parent.verticalCenter
      spacing: Theme.paddingLarge
      onVisibleChanged: {
        // unsubscribe from push when data is not displayed anyway
        if (visible) {
          listener_current.subscribe()
        } else {
          listener_current.unsubscribe()
        }
      }

      property var batt_charge_current

      ContextProperty {
        id: listener_current
        key: "Battery.Current"
        value: 0
        onValueChanged: notChargingInfo.opacity = (Math.round(
                                                     value / 1000) === 0 ? 1 : 0)
      }

      Label {
        id: notChargingInfo
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: qsTr(
                "The external power source will continue to feed your device - only battery charging is stopped.")
      }

      //no TextSwitch on purpose, I think a button looks better here
      Button {
        anchors.horizontalCenter: parent.horizontalCenter
        text: cppModel.charging ? qsTr("Disable charging") : qsTr(
                                    "Enable charging")
        preferredWidth: Theme.buttonWidthLarge
        onClicked: cppModel.charging = !cppModel.charging
      }

      BatteryInfo {
        anchors.horizontalCenter: parent.horizontalCenter
      }
    }
  }
}
