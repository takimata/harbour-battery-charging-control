import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {

  Image {
    // svg not supported
    source: "../../images/background.png"
    anchors {
      verticalCenter: parent.verticalCenter

      top: parent.top
      topMargin: Theme.paddingMedium

      right: parent.right
      rightMargin: Theme.paddingMedium
    }
    fillMode: Image.PreserveAspectFit
    opacity: 0.3
  }

  Column {
    width: parent.width
    anchors.verticalCenter: parent.verticalCenter
    spacing: Theme.paddingMedium

    Label {
      font.pixelSize: Theme.fontSizeMedium
      color: Theme.secondaryColor
      anchors.horizontalCenter: parent.horizontalCenter
      text: cppModel.charging ? qsTr("Charging") : qsTr("Discharging")
    }

    Label {
      font.pixelSize: Theme.fontSizeLarge
      anchors.horizontalCenter: parent.horizontalCenter
      text: cppModel.currentSoC + "%"
    }

    Label {
      font.pixelSize: Theme.fontSizeMedium
      color: Theme.secondaryColor
      anchors.horizontalCenter: parent.horizontalCenter
      text: qsTr("till limit:")
      visible: cppModel.charging & cppModel.automaticMode
    }

    Label {
      font.pixelSize: Theme.fontSizeLarge
      anchors.horizontalCenter: parent.horizontalCenter
      text: cppModel.upperSoCLimit + "%"
      visible: cppModel.charging & cppModel.automaticMode
    }

    Label {
      //just for spacing
      font.pixelSize: Theme.fontSizeLarge
      anchors.horizontalCenter: parent.horizontalCenter
      visible: !(cppModel.charging & cppModel.automaticMode)
      text: " "
    }

    Label {
      font.pixelSize: Theme.fontSizeMedium
      color: Theme.secondaryColor
      anchors.horizontalCenter: parent.horizontalCenter
      text: qsTr("Temperature") + ":"
    }

    Label {
      font.pixelSize: Theme.fontSizeLarge
      anchors.horizontalCenter: parent.horizontalCenter
      text: formatFloat(cppModel.batteryTemperatureInDecaCelsius / 10, 1) + " °C"
    }
  }
}
