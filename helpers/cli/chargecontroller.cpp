#include <cstring>
#include <cerrno>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string_view>
#include <string>

#include "chargecontroller.hpp"

using namespace std;

constexpr string_view BATT_CHG_ENABLE_XPERIA_X = "/sys/class/power_supply/battery/battery_charging_enabled";
constexpr string_view BATT_CHG_ENABLE_XPERIA_10_PLUS = "/sys/class/power_supply/battery/charging_enabled";
constexpr string_view BATT_CHG_ENABLE_XPERIA_10_II   = "/sys/class/power_supply/battery/charging_enabled";

// devices where power supply doesn't supply power without charging:
// XPERIA_10_II

constexpr string_view LRC_ENABLE_XPERIA_X     = "/sys/class/power_supply/battery/lrc_enable";
constexpr string_view LRC_ENABLE_XPERIA_10_II = "/sys/class/power_supply/battery_ext/lrc_enable";

constexpr string_view LRC_SOCMAX_XPERIA_X     = "/sys/class/power_supply/battery/lrc_socmax";
constexpr string_view LRC_SOCMAX_XPERIA_10_II = "/sys/class/power_supply/battery_ext/lrc_socmax";

int set_property_value(std::string_view sysfspath, std::string_view value) {
  ofstream sysfsfile;
  sysfsfile.open(std::filesystem::path(sysfspath), ios::out | ios::trunc );
  if (!sysfsfile.is_open()) {
    std::cerr << "Error opening " << sysfspath << endl;
    return EXIT_FAILURE;
  }
  sysfsfile << value << endl;

  if (sysfsfile.good()) {
    return EXIT_SUCCESS;
  } else {
    std::cerr << "Error writing to " << sysfspath << endl;
    return EXIT_FAILURE;
  }
}

ChargeControllerI::ChargeControllerI(const std::string_view batt_chg_enable, const std::optional<const struct LRC> lrc) :
  BATT_CHG_ENABLE(batt_chg_enable), lrc(lrc) {
}

ChargeControllerXperia_X::ChargeControllerXperia_X() :
  ChargeControllerI(BATT_CHG_ENABLE_XPERIA_X,
                    std::optional<struct LRC>{{.ENABLE = LRC_ENABLE_XPERIA_X, .SOCMAX = LRC_SOCMAX_XPERIA_X}}) {
}

ChargeControllerXperia_10_II::ChargeControllerXperia_10_II() :
  ChargeControllerI(BATT_CHG_ENABLE_XPERIA_10_II,
                    std::optional<struct LRC>{{.ENABLE = LRC_ENABLE_XPERIA_10_II, .SOCMAX = LRC_SOCMAX_XPERIA_10_II}}) {
}

ChargeControllerXperia_10_Plus::ChargeControllerXperia_10_Plus() :
  ChargeControllerI(BATT_CHG_ENABLE_XPERIA_10_PLUS,
                    std::nullopt) {
}

bool ChargeControllerXperia_X::matchDevice() {
  return std::filesystem::exists(BATT_CHG_ENABLE_XPERIA_X) && std::filesystem::exists(LRC_ENABLE_XPERIA_X);
}

bool ChargeControllerXperia_10_II::matchDevice() {
  return std::filesystem::exists(BATT_CHG_ENABLE_XPERIA_10_II) && std::filesystem::exists(LRC_ENABLE_XPERIA_10_II);
}

bool ChargeControllerXperia_10_Plus::matchDevice() {
  return std::filesystem::exists(BATT_CHG_ENABLE_XPERIA_10_PLUS) &&
         !std::filesystem::exists("/sys/class/power_supply/battery_ext/") &&
         !std::filesystem::exists("/sys/class/power_supply/battery/lrc_enable");
}

int ChargeControllerI::toggle_charging(bool enable) {
  return set_property_value(BATT_CHG_ENABLE, enable ? "1" : "0");
}

int ChargeControllerI::limited_range_charge_charging(unsigned int upper_limit) {
  if (lrc.has_value()) {
    int rc = this->toggle_charging(true);  // required for LRC to be effective
    rc |= set_property_value(lrc.value().SOCMAX, std::to_string(upper_limit));
    // need to re-enable LRC so that the kernel picks up a new (possibly higher) limit
    rc |= set_property_value(lrc.value().ENABLE, "0");
    rc |= set_property_value(lrc.value().ENABLE, "1");
    return rc;
  } else {
    return ENOTSUP;
  }
}

int ChargeControllerI::limited_range_charge_disable() {
  if (lrc.has_value()) {
    return set_property_value(lrc.value().ENABLE, "0");
  } else {
    return ENOTSUP;
  }
}

bool ChargeControllerI::supports_kernel_lrc() {
  return lrc.has_value();
}
