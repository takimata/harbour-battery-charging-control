import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.1
import org.freedesktop.contextkit 1.0

import "../panes"

Page {
  id: page
  allowedOrientations: Orientation.All

  SilicaFlickable {
    anchors.fill: parent

    // content.height includes pageheader.height, so don't add it here
    contentHeight: content.height
    VerticalScrollDecorator {}

    PullDownMenu {
      MenuItem {
        text: qsTr("Manual mode")
        onClicked: {
          cppModel.automaticMode = false
          settings.automaticMode = false
          pageStack.replaceAbove(null, Qt.resolvedUrl("ManualMode.qml"), null,
                                 PageStackAction.Immediate)
        }
      }
    }

    PageHeader {
      id: pageheader
      title: qsTr("Automatic charging control")
    }

    Column {
      id: content
      spacing: Theme.paddingLarge
      width: parent.width

      onVisibleChanged: {
        // unsubscribe from push when data is not displayed anyway
        if (visible) {
          listener_current.subscribe()
        } else {
          listener_current.unsubscribe()
        }
      }

      // TODO center content vertically and keep scroll bar in landscape mode
      // use this as workaround for now
      Rectangle {
        width: parent.width
        height: pageheader.height + Theme.paddingMedium
        opacity: 0
      }

      Label {
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: qsTr(
                "Charging will be automatically disabled if the battery level reaches the upper limit and enabled if it drops below this limit.")
      }

      // would fit better here but label text is not centered
      // ProgressBar {
      //   width: parent.width
      //   anchors.horizontalCenter: parent.horizontalCenter
      //   minimumValue: 0
      //   maximumValue: 100
      //   value: cppModel.currentSoC
      //   valueText: value + "%"
      //   label: qsTr("Current battery level")
      // }
      ContextProperty {
        id: listener_current
        key: "Battery.Current"
        value: 0
        onValueChanged: notChargingInfo.opacity = (Math.round(
                                                     value / 1000) === 0 ? 1 : 0)
      }

      Label {
        id: notChargingInfo
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: qsTr(
                "The external power source will continue to feed your device - only battery charging is stopped.")
      }

      Slider {
        id: sliderUpperLimit
        label: qsTr("Upper Limit")
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        //I think 50% instead of 0% as minimal limit is reasonable
        //rationale: smaller range allows faster value selection
        minimumValue: 50
        maximumValue: 100
        stepSize: 1
        // set initial value
        Component.onCompleted: value = cppModel.upperSoCLimit
        valueText: value + "%"
        onReleased: {
          cppModel.upperSoCLimit = this.value
          settings.upperLimit = this.value
        }
      }

      Slider {
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        enabled: false
        handleVisible: false
        minimumValue: 0
        maximumValue: 100
        stepSize: 1
        value: cppModel.currentSoC
        valueText: cppModel.currentSoC + "%"
        label: qsTr("Current battery level")
      }

      BatteryInfo {
        anchors.horizontalCenter: parent.horizontalCenter
      }

      Label {
        width: parent.width - 2 * Theme.horizontalPageMargin
        wrapMode: Text.Wrap
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        text: cppModel.kernelSupportsLRC ? qsTr(
                                             "You can close the app now, your device will stop charging by itself until next reboot.") : qsTr(
                                             "This device doesn't support automatic charge control, so please keep the app running.")
      }
    }
  }
}
