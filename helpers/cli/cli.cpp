#include <charconv>
#include <cstring>
#include <cerrno>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>

#include "chargecontroller.hpp"
using namespace std;

std::unique_ptr<ChargeControllerI> getController() {
  if (ChargeControllerXperia_X::matchDevice()) {
    return std::make_unique<ChargeControllerXperia_X>();
  } else if (ChargeControllerXperia_10_II::matchDevice()) {
    return std::make_unique<ChargeControllerXperia_10_II>();
  } else if (ChargeControllerXperia_10_Plus::matchDevice()) {
    return std::make_unique<ChargeControllerXperia_10_Plus>();
  } else {
    std::cerr << "Could not determine device model" << std::endl;
    exit(EXIT_FAILURE);
  }
}

/**
 * @brief Device-independent control of charging
 * @param enable
 * @return success / exit code
 */
int toggle_charging(bool enable) {
  std::unique_ptr<ChargeControllerI> c = getController();
  return c->toggle_charging(enable);
}

int main(int argc, char *argv[]) {

  if (argc != 2) {
    std::cerr << "Usage: " << ((argc > 0) ? argv[0] : "") << " OPTION" << std::endl;
    std::cerr << "where OPTION can be: " << std::endl;
    std::cerr << " has-kernel-lrc    exit code is 0 if device supports kernel-controlled charge limit" << std::endl;
    std::cerr << " on                enable charging" << std::endl;
    std::cerr << " off               disable charging" << std::endl;
    std::cerr << " <LRC_MAX_LIMIT_IN_PERCENT> percentage where to stop charging" << std::endl;
    std::cerr << " lrcoff            disable kernel-controlled charging" << std::endl;
    exit(EXIT_FAILURE);
  }

  unsigned int lrc_max_percentage = 0;

  if (std::strcmp(argv[1], "has-kernel-lrc") == 0) {
    return getController()->supports_kernel_lrc() ? 0 : ENOTSUP;
  } else if (std::strcmp(argv[1], "on") == 0) {
    return toggle_charging(true);
  } else if (std::strcmp(argv[1], "off") == 0) {
    return toggle_charging(false);
  } else if (auto [p, ec] = std::from_chars(argv[1], argv[1] + strlen(argv[1]), lrc_max_percentage); ec == std::errc()) {
    if (lrc_max_percentage > 100) {
      lrc_max_percentage = 100;
    }
    return getController()->limited_range_charge_charging(lrc_max_percentage);
  } else if (std::strcmp(argv[1], "lrcoff") == 0) {
    return getController()->limited_range_charge_disable();
  } else {
    std::cerr << "Could not parse arguments" << std::endl;
    return EXIT_FAILURE;
  }
}
