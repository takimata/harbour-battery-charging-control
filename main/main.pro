# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed
# The name of your application
TARGET = harbour-battery-charging-control

# removing sailfishapp_i18n will disable building translations every time
CONFIG += sailfishapp sailfishapp_i18n
#CONFIG += c++17 # doesn't work with current Qt
QMAKE_CXXFLAGS += -std=c++17
QMAKE_CXXFLAGS += -Wall -Wextra -Werror -Wmissing-include-dirs -Wredundant-decls -Woverloaded-virtual -Wundef -Wctor-dtor-privacy -Wcast-align -Wcast-qual -Wswitch-default -Wswitch-enum -Wfloat-equal -Wold-style-cast
QMAKE_CXXFLAGS += -lstdc++fs
LIBS += -lkeepalive
DEFINES += APPLICATION_NAME=\'\"$$TARGET\"\'

HEADERS += \
  src/controllerModel.hpp \
  src/settings.hpp

SOURCES += src/harbour-battery-charging-control.cpp \
    src/controllerModel.cpp \
    src/settings.cpp

DISTFILES += qml/harbour-battery-charging-control.qml \
    qml/cover/CoverPage.qml \
    qml/pages/AutomaticMode.qml \
    qml/pages/ManualMode.qml \
    qml/pages/Error.qml \
    qml/panes/BatteryInfo.qml \
    translations/*.ts \
    images/background.png \
    harbour-battery-charging-control.desktop \
    harbour-battery-charging-control.pro.user

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

coverimage.files = images
coverimage.path = /usr/share/$${TARGET}/
INSTALLS += coverimage

# btw, localized app name is in the .desktop file
TRANSLATIONS += translations/harbour-battery-charging-control-de.ts translations/harbour-battery-charging-control-sv.ts

OTHER_FILES += icons/generateIcons.sh icons/icon.svg
