#include <sailfishapp.h>
#include <QtQuick>
#include <QObject>
#include <QQmlEngine>
#include <QScopedPointer>
#include "controllerModel.hpp"
#include "settings.hpp"

int main(int argc, char *argv[]) {
  QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
  app->setOrganizationName(QStringLiteral(APPLICATION_NAME));
  app->setApplicationName(QStringLiteral(APPLICATION_NAME));
  QScopedPointer<QQuickView> view(SailfishApp::createView());

  Settings settings;
  view->rootContext()->setContextProperty("settings", &settings);  // QStringLiteral here breaks autocomplete in QML

  ControllerModel cm{settings};
  view->rootContext()->setContextProperty("cppModel", &cm);

  view->setSource(SailfishApp::pathTo(QStringLiteral("qml/harbour-battery-charging-control.qml")));
  view->show();
  return app->exec();
}
