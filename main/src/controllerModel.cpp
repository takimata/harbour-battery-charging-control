#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <QtDebug>
#include <QList>

#include "controllerModel.hpp"

constexpr char const *CLI_BIN_NAME = "harbour-battery-charging-control-cli";

ControllerModel::ControllerModel(Settings& cfg, QObject *parent) :
  QObject(parent),
  upperSoCLimit(cfg.getUpperLimit()),
  automaticMode(cfg.getAutomaticMode()),
  kernelSupportsLRC(lazy_value_or_else(cfg.getCachedKernelSupportsLRC(), [] {return isKernelSupportsLRC();})),
  chargeActivity(this) {
  chargeActivity.setWakeupFrequency(BackgroundActivity::TenMinutes);
  connect(&chargeActivity, SIGNAL(running()), this, SLOT(bg_activity()));

  if (!cfg.getCachedKernelSupportsLRC()) {
    cfg.setCachedKernelSupportsLRC(kernelSupportsLRC);
  }

  applyCurrentChargingPolicy();
}

int ControllerModel::call_cli(const char *const argv[]) {
  pid_t pid = fork();

  if (pid == -1) {
    return errno;
  } else if (pid == 0) {
    // Casting away const-ness is okay because the standard promises that argv is constant
    execvp(CLI_BIN_NAME, const_cast<char *const *>(argv));
    exit(errno);
  } else {
    int child_exitcode;
    if (waitpid(pid, &child_exitcode, 0) == -1) {
      return errno;
    } else {
      return child_exitcode;
    }
  }
}

bool ControllerModel::isKernelSupportsLRC() {
  int ret = call_cli((const char *const[]) {CLI_BIN_NAME, "has-kernel-lrc", NULL});
  return (ret == 0) ? true : false;
}

int ControllerModel::enableCharging(bool enable) {
  qDebug() << "Charging enabled: " << enable;
  const char *const argv[] = {CLI_BIN_NAME, enable ? "on" : "off", NULL};
  return call_cli(argv);
}

int ControllerModel::limited_range_charge_charging(unsigned int upper_limit) {
  std::string limit = std::to_string(upper_limit);
  const char *const argv[] = {CLI_BIN_NAME, limit.c_str(), NULL};
  return call_cli(argv);
}

int ControllerModel::limited_range_charge_disable() {
  return call_cli((const char *const[]) {CLI_BIN_NAME, "lrcoff", NULL});
}

void ControllerModel::bg_activity() {
  if (currentSoC >= upperSoCLimit) {
    enableCharging(false);
    chargeActivity.stop();
  } else {
    enableCharging(true);
    chargeActivity.wait();
  }
}

/** Enables or disables charging depending on the current state
 * @brief ControllerModel::applyCurrentChargingPolicy
 */
void ControllerModel::applyCurrentChargingPolicy() {
  if (automaticMode) {
    if (kernelSupportsLRC) {
      this->limited_range_charge_charging(upperSoCLimit);
    } else {
      bg_activity();
    }
  } else {
    if (kernelSupportsLRC) {
      // manual mode enabled
      this->limited_range_charge_disable();
    }
  }
}

void ControllerModel::setCurrentChargingState(const QString& state) {
  qDebug() << "Charging State: " << state;
  ChargingState newValue = chargingStateMapping.value(state, ChargingState::Unknown);

  if (newValue != chargingState) {
    chargingState = newValue;
    emit isChargingChanged();
  }
}

void ControllerModel::setCurrentExternalPowerSupply(const QString& powerSupply) {
  qDebug() << "Charger type: " << powerSupply;

  bool powerSupplyPreviouslyPresent = powerSupplyPresent;
  if (powerSupply == "None") {
    powerSupplyPresent = false;  // empty charger type means no power source
    if (!kernelSupportsLRC) {
      qDebug() << "chargeActivity stopped";
      chargeActivity.stop();
    }
  } else {
    powerSupplyPresent = true;
    if (!kernelSupportsLRC) {
      qDebug() << "chargeActivity enabled";
      chargeActivity.wait();
    }
  }
  if (powerSupplyPreviouslyPresent != powerSupplyPresent) {
    emit chargerConnectedChanged();
  }
}

void ControllerModel::setAutomaticMode(bool value) {
  if (value == automaticMode) {
    return;
  }
  automaticMode = value;
  applyCurrentChargingPolicy();
  emit automaticModeChanged();
}

void ControllerModel::setUpperLimit(uint soc_percentage) {
  if (upperSoCLimit == soc_percentage) {
    return;
  }
  soc_percentage = (soc_percentage > 100) ? 100 : soc_percentage;
  upperSoCLimit  = soc_percentage;
  applyCurrentChargingPolicy();
  emit upperLimitChanged();
}

void ControllerModel::setCurrentChargePercentage(uint new_soc_percentage) {
  qDebug() << "setCurrentChargePercentage: " << new_soc_percentage;
  new_soc_percentage = (new_soc_percentage > 100) ? 100 : new_soc_percentage;
  /* In case we are satisfactorily charged, the BackgroundActivity is stopped.
   * If we now temporarily loose some charge due to heavy load, we might want to charge again.
   * As all of this can happen while connected to a charger, we have to look at the current SOC.
   */
  if (!kernelSupportsLRC && (new_soc_percentage < currentSoC)) {
    bg_activity();
  }

  if (currentSoC == new_soc_percentage) {
    return;
  }
  currentSoC = new_soc_percentage;
  emit currentValueChanged();
}

void ControllerModel::setCurrentBatteryTemperatureInDecaCelsius(int decacelsius) {
  if (decacelsius != batteryTemperatureInDecaCelsius) {
    batteryTemperatureInDecaCelsius = decacelsius;
    emit batteryTemperatureInDecaCelsiusChanged();
  }
}
