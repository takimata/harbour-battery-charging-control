<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>AutomaticMode</name>
    <message>
        <source>Manual mode</source>
        <translation>Manueller Modus</translation>
    </message>
    <message>
        <source>Upper Limit</source>
        <translation>Oberes Limit</translation>
    </message>
    <message>
        <source>Current battery level</source>
        <translation>Aktueller Akkustand</translation>
    </message>
    <message>
        <source>Automatic charging control</source>
        <translation>Automatische Ladekontrolle</translation>
    </message>
    <message>
        <source>Charging will be automatically disabled if the battery level reaches the upper limit and enabled if it drops below this limit.</source>
        <translation>Wenn das obere Limit erreicht ist, wird das Aufladen deaktiviert. Fällt der Akkustand wieder darunter, wird wieder aufgeladen.</translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation>Das Ladegerät versorgt das Gerät weiter mit Strom, nur das Aufladen ist gestoppt.</translation>
    </message>
    <message>
        <source>This device doesn&apos;t support automatic charge control, so please keep the app running.</source>
        <translation>Dein Gerät unterstützt keine vollautomatische Ladekontrolle, weswegen die App im Hintergrund weiter laufen muss.</translation>
    </message>
    <message>
        <source>You can close the app now, your device will stop charging by itself until next reboot.</source>
        <translation>Du kannst die App nun schließen, das Laden wird nun vollautomatisch gesteuert (bis zum nächsten Reboot).</translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <source>Battery voltage</source>
        <translation>Akkuspannung</translation>
    </message>
    <message>
        <source>Charge current</source>
        <translation>Ladestrom</translation>
    </message>
    <message>
        <source>Charging...</source>
        <translation>Lade...</translation>
    </message>
    <message>
        <source>Discharging...</source>
        <translation>Entlade...</translation>
    </message>
    <message>
        <source>Not charging</source>
        <translation>Aufladen gestoppt</translation>
    </message>
    <message>
        <source>Please report a bug: Your device is charging but no power supply seems to be connected!</source>
        <translation>Bitte melde einen Fehler: Dein Gerät scheint ohne Ladegerät zu laden!</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Charging</source>
        <translation>Lade</translation>
    </message>
    <message>
        <source>Discharging</source>
        <translation>Entlade</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <source>till limit:</source>
        <translation>bis Limit:</translation>
    </message>
</context>
<context>
    <name>Error</name>
    <message>
        <source>Something went wrong during startup.</source>
        <translation>Beim Starten ist etwas schiefgelaufen.</translation>
    </message>
</context>
<context>
    <name>ManualMode</name>
    <message>
        <source>Automatic mode</source>
        <translation>Automatischer Modus</translation>
    </message>
    <message>
        <source>Manual charging control</source>
        <translation>Manuelle Ladekontrolle</translation>
    </message>
    <message>
        <source>Disable charging</source>
        <translation>Stoppe Aufladen</translation>
    </message>
    <message>
        <source>Enable charging</source>
        <translation>Erlaube Aufladen</translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation>Das Ladegerät versorgt das Gerät weiter mit Strom, nur das Aufladen ist gestoppt.</translation>
    </message>
</context>
</TS>
