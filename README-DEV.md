# Preface

This application should work with all Sailfish devices where the sysfs files `/sys/class/power_supply/battery/lrc_enable` or
`/sys/class/power_supply/battery_ext/lrc_enable` are present.


# How it works

**Automatic mode (recommended):** Use the LRC functionality from the kernel. When the limit is reached, charging is stopped (charging current = 0) and the charger only supplies the power needed to run the device.

**Manual mode:** Use the kernel to stop charging completely.
Depending on the device (e.g., with Xperia 10 II), this may drain the battery even though there is still a charger connected.
Maybe this can be fixed at the kernel-level, but that needs more investigation.


Restricting the charge current is possible on some devices but not implemented here (yet).

# Battery charging-related acronyms
## SOC - State of charge

Percentage to which the battery is currently charged

## LRC - Limit range charge

Kernel-controlled limited charging

## FCC - Fast charge current
## ICL - Input current limit
