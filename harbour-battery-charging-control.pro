TEMPLATE = subdirs

cli.subdir = helpers/cli

main.depends = cli

SUBDIRS += cli \
    main

# show rpm dir in project explorer
OTHER_FILES += $$files(rpm/*) README.md

