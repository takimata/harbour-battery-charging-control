import QtQuick 2.0
import Sailfish.Silica 1.0
import org.freedesktop.contextkit 1.0
import "pages"

ApplicationWindow {

  ContextProperty {
    key: "Battery.ChargePercentage"
    value: 0 // set optional default value before we get an updated value
    onValueChanged: cppModel.setCurrentChargePercentage(value)
  }

  ContextProperty {
    key: "Battery.ChargingState" // like charging, discharging
    value: ""
    onValueChanged: cppModel.setCurrentChargingState(value)
  }

  ContextProperty {
    key: "Battery.ChargerType" // like None, USB
    value: ""
    onValueChanged: cppModel.setCurrentExternalPowerSupply(value)
  }

  ContextProperty {
    key: "Battery.Temperature"
    value: 0
    onValueChanged: cppModel.setCurrentBatteryTemperatureInDecaCelsius(value)
  }

  Component {
    id: automaticMode
    AutomaticMode {}
  }
  Component {
    id: manualMode
    ManualMode {}
  }

  initialPage: cppModel.automaticMode ? automaticMode : manualMode
  cover: Qt.resolvedUrl("cover/CoverPage.qml")
  allowedOrientations: defaultAllowedOrientations

  // round and format with given number of decimal places
  function formatFloat(value, decimal_places) {
    return Number(value).toLocaleString(Qt.locale(), "f", decimal_places)
  }

  function round(value, decimal_places) {
    return Number(Math.round(
                    value + ('e' + decimal_places)) + ('e-' + decimal_places))
  }
}
