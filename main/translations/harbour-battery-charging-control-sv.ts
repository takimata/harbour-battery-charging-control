<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv" sourcelanguage="en">
<context>
    <name>AutomaticMode</name>
    <message>
        <source>Manual mode</source>
        <translation>Manuellt läge</translation>
    </message>
    <message>
        <source>Upper Limit</source>
        <translation>Övre gräns</translation>
    </message>
    <message>
        <source>Current battery level</source>
        <translation>Aktuell batterinivå</translation>
    </message>
    <message>
        <source>Automatic charging control</source>
        <translation>Automatisk laddningskontroll</translation>
    </message>
    <message>
        <source>Charging will be automatically disabled if the battery level reaches the upper limit and enabled if it drops below this limit.</source>
        <translation>Laddning inaktiveras automatiskt om batterinivån når den övre gränsen och aktiveras om den sjunker under denna gräns.</translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation>Den externa strömkällan fortsätter att mata din enhet. - Endast batteriladdning stoppas.</translation>
    </message>
    <message>
        <source>This device doesn&apos;t support automatic charge control, so please keep the app running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can close the app now, your device will stop charging by itself until next reboot.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <source>Battery voltage</source>
        <translation>Batterispänning</translation>
    </message>
    <message>
        <source>Charge current</source>
        <translation>Laddström</translation>
    </message>
    <message>
        <source>Charging...</source>
        <translation>Laddar...</translation>
    </message>
    <message>
        <source>Discharging...</source>
        <translation>Laddar ur...</translation>
    </message>
    <message>
        <source>Not charging</source>
        <translation>Ingen laddning</translation>
    </message>
    <message>
        <source>Please report a bug: Your device is charging but no power supply seems to be connected!</source>
        <translation>Posta en felrapport! Din enhet laddas men ingen strömförsörjning verkar vara ansluten!</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Charging</source>
        <translation>Laddar upp</translation>
    </message>
    <message>
        <source>Discharging</source>
        <translation>Laddar ur</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Temperatur</translation>
    </message>
    <message>
        <source>till limit:</source>
        <translation>till gränsen:</translation>
    </message>
</context>
<context>
    <name>Error</name>
    <message>
        <source>Something went wrong during startup.</source>
        <translation>Något gick fel vid uppstarten.</translation>
    </message>
</context>
<context>
    <name>ManualMode</name>
    <message>
        <source>Automatic mode</source>
        <translation>Automatiskt läge</translation>
    </message>
    <message>
        <source>Manual charging control</source>
        <translation>Manuell laddningskontroll</translation>
    </message>
    <message>
        <source>Disable charging</source>
        <translation>Avaktivera uppladdning</translation>
    </message>
    <message>
        <source>Enable charging</source>
        <translation>Aktivera uppladdning</translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation>Den externa strömkällan fortsätter att mata din enhet. - Endast batteriladdning stoppas.</translation>
    </message>
</context>
</TS>
