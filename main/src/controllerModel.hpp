#ifndef CONTROLLERMODEL_H
#define CONTROLLERMODEL_H
#include <memory>

#include <QtCore>
#include <QMap>
#include <QtDebug>

#include <keepalive/backgroundactivity.h>  // see https://github.com/sailfishos/nemo-keepalive for doc

#include "settings.hpp"

template<typename T, typename F>
T lazy_value_or_else(const std::optional<T> &opt, F fn) {
  return (opt) ? opt.value() : fn();
}

class ControllerModel : public QObject {
  Q_OBJECT

  Q_PROPERTY(uint upperSoCLimit READ getUpperLimit WRITE setUpperLimit NOTIFY upperLimitChanged)
  Q_PROPERTY(uint currentSoC READ getCurrentValue NOTIFY currentValueChanged)
  Q_PROPERTY(bool charging READ isCharging WRITE enableCharging NOTIFY isChargingChanged)
  Q_PROPERTY(bool automaticMode READ getAutomaticMode WRITE setAutomaticMode NOTIFY automaticModeChanged)
  Q_PROPERTY(bool kernelSupportsLRC READ getKernelSupportsLRC NOTIFY kernelSupportsLRCChanged)
  Q_PROPERTY(bool chargerConnected READ isChargerConnected NOTIFY chargerConnectedChanged)
  Q_PROPERTY(int batteryTemperatureInDecaCelsius READ getBatteryTemperatureInDecaCelsius NOTIFY batteryTemperatureInDecaCelsiusChanged)

  private slots:
  void bg_activity();

  private:
  enum ChargingState {Unknown, Charging, Discharging, Idle};
  QMap<QString, ChargingState> chargingStateMapping = {std::make_pair("unknown", ChargingState::Unknown),
                                                       std::make_pair("charging", ChargingState::Charging),
                                                       std::make_pair("discharging", ChargingState::Discharging),
                                                       std::make_pair("idle", ChargingState::Idle)};

  ChargingState chargingState = ChargingState::Unknown;
  bool powerSupplyPresent     = false;
  int batteryTemperatureInDecaCelsius = 0;
  // (configured limit of) state of charge in percent
  uint upperSoCLimit = 80, currentSoC = 0;
  bool automaticMode = true;
  const bool kernelSupportsLRC;

  BackgroundActivity chargeActivity;

  void applyCurrentChargingPolicy();

  public:
  explicit ControllerModel(Settings& settings, QObject *parent = 0);

  bool isCharging() {
    return (chargingState == ChargingState::Charging);
  }

  bool isChargerConnected() {
    return powerSupplyPresent;
  }

  int getBatteryTemperatureInDecaCelsius() {
    return batteryTemperatureInDecaCelsius;
  }

  bool getAutomaticMode() {
    return automaticMode;
  }

  uint getUpperLimit() const {
    return upperSoCLimit;
  }
  uint getCurrentValue() const {
    return currentSoC;
  }

  bool getKernelSupportsLRC() {
    return kernelSupportsLRC;
  }

  /** Jump through some hoops to avoid system()
   * @brief call_cli A replacement for system()
   * @param argv
   * @return child exir code
   */
  static int call_cli(const char *const argv[]);

  static bool isKernelSupportsLRC();
  static int enableCharging(bool enable);
  static int limited_range_charge_charging(unsigned int upper_limit);
  static int limited_range_charge_disable();
  void setAutomaticMode(bool v);
  void setUpperLimit(uint value);

  public slots:
  /// update battery charge state: percentage \in [0, 100]
  void setCurrentChargePercentage(uint percentage);
  /** whether we are charging or not
   * @brief setCurrentChargingState
   * @param state usually \in {unknown, charging, discharging, idle}
   * idle means full battery
   * see https://git.sailfishos.org/mer-core/nemo-qml-plugin-contextkit/blob/master/src/providers/Battery.qml
   */
  void setCurrentChargingState(const QString& state);

  /** To be called when external power source changes
   * @brief setCurrentExternalPowerSupply
   * @param powerSupply type of power source (excluding battery):  x \in {USB, DCP, HVDCP, CDP, Other, Wireless, None}
   */
  void setCurrentExternalPowerSupply(const QString& powerSupply);
  void setCurrentBatteryTemperatureInDecaCelsius(int decacelsius);

  signals:
  void upperLimitChanged();
  void currentValueChanged();
  void isChargingChanged();
  void automaticModeChanged();
  void kernelSupportsLRCChanged();
  void chargerConnectedChanged();
  void batteryTemperatureInDecaCelsiusChanged();
};

#endif  // CONTROLLERMODEL_H
