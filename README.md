# Preface

In order to prolong the battery's life, this application allows to stop battery charging while the device is still connected to a power source (charger, computer USB, ...).

For lithium-ion batteries, typical recommendations are to stop charging in the range 70-85% to avoid stressing the battery.
Also, you should avoid a complete discharge or in general, states below 10-20% charge.
Using fast chargers negatively impacts the lifetime of your battery, too.

This application with the creative name "Battery Charging Control" provides a very lightweight approach to extend the lifetime of you Li-ion battery by stopping charging at user-defined levels.

## Tested with

* Sony Xperia X

  On Xperia X devices, the value of the charge current only refreshes ~2 per minute. Works fine apart from that. Be a bit patient.
* Sony Xperia 10 II


# How it works

Battery charging is entirely controlled by the kernel, which can be configured via some sysfs files from userspace (which requires elevated privileges).

This means that you don't have to run the app/some background service all the time.
Just configure your desired threshold, the application instructs the kernel appropriately, and you can close the app again (that is, until the kernel forgets your settings after a reboot).

For security reasons, sysfs access is done by a separate suid CLI binary (which could also be used to control charging by a script, if you want to).

More details: see [here](README-DEV.md)
