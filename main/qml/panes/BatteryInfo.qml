import QtQuick 2.0
import Sailfish.Silica 1.0
import org.freedesktop.contextkit 1.0
import org.nemomobile.systemsettings 1.0

Column {
  width: parent.width
  spacing: Theme.paddingLarge
  onVisibleChanged: {
    // unsubscribe from push when data is not displayed anyway
    if (visible) {
      listener_temperature.subscribe()
      listener_current.subscribe()
      listener_voltage.subscribe()
    } else {
      listener_temperature.unsubscribe()
      listener_current.unsubscribe()
      listener_voltage.unsubscribe()
    }
  }

  property var batt_temperature_in_decacelsius
  property var batt_charge_current
  property var batt_voltage

  ContextProperty {
    id: listener_temperature
    key: "Battery.Temperature"
    value: 0
    onValueChanged: batt_temperature_in_decacelsius = value
  }

  ContextProperty {
    id: listener_voltage
    key: "Battery.Voltage"
    value: 0
    onValueChanged: batt_voltage = value
  }

  DeviceInfo {
    id: deviceInfo
  }

  ContextProperty {
    id: listener_current
    key: "Battery.Current"
    value: 0
    onValueChanged: {
      if (deviceInfo.designation == "f5121"
          || deviceInfo.designation == "f5122") {
        // Xperia X: current is reported with a wrong sign
        batt_charge_current = -value
      } else {
        batt_charge_current = value
      }
    }
  }

  DetailItem {
    anchors.horizontalCenter: parent.horizontalCenter
    label: qsTr("State")
    value: getStateText(cppModel.charging, cppModel.chargerConnected,
                        batt_charge_current / 1000)
  }

  DetailItem {
    anchors.horizontalCenter: parent.horizontalCenter
    label: qsTr("Temperature")
    value: formatFloat(batt_temperature_in_decacelsius / 10, 1) + " °C"
  }

  DetailItem {
    anchors.horizontalCenter: parent.horizontalCenter
    label: qsTr("Battery voltage")
    value: formatFloat(batt_voltage + "e-6", 2) + " V"
  }

  DetailItem {
    label: qsTr("Charge current")
    value: Math.round(batt_charge_current / 1000) + " mA"
  }

  function getStateText(isCharging, isChargerConnected, batt_charge_current) {
    if (isChargerConnected) {
      if (isCharging) {
        return qsTr("Charging...")
      } else {
        var current = round(batt_charge_current, 0)
        if (current === 0) {
          return qsTr("Not charging")
        } else if (current > 0) {
          return qsTr("Charging...")
        } else {
          return qsTr("Discharging...")
        }
      }
    } else {
      if (isCharging) {
        return qsTr(
              "Please report a bug: Your device is charging but no power supply seems to be connected!")
      } else {
        return qsTr("Discharging...")
      }
    }
  }
}
