#include <optional>
#include "settings.hpp"

// keys for settings
const QString SK_Auto = QStringLiteral("AutomaticMode");
const QString SK_upperLimit = QStringLiteral("StopChargingAt");
const QString SK_KernelSupportsLRC = QStringLiteral("CachedDeviceKernelSupportsLRC");

Settings::Settings(QObject *parent) : QObject(parent), settings(QStringLiteral(APPLICATION_NAME), QStringLiteral(APPLICATION_NAME), this) {
}

bool Settings::getAutomaticMode() const {
  return settings.value(SK_Auto, QVariant(true)).toBool();
}

void Settings::setAutomaticMode(bool isAuto) {
  bool changed = false;
  if (isAuto != getAutomaticMode()) {
    changed = true;
  }
  settings.setValue(SK_Auto, isAuto);
  if (changed) {
    emit automaticModeChanged();
  }
}

uint Settings::getUpperLimit() const {
  return settings.value(SK_upperLimit, QVariant(80)).toUInt();
}

void Settings::setUpperLimit(uint chargeLimit) {
  bool changed = false;
  if (chargeLimit != getUpperLimit()) {
    changed = true;
  }
  settings.setValue(SK_upperLimit, chargeLimit);
  if (changed) {
    emit upperLimitChanged();
  }
}

std::optional<bool> Settings::getCachedKernelSupportsLRC() const {
  QVariant val = settings.value(SK_KernelSupportsLRC);
  return val.isNull() ? std::nullopt : std::optional<bool>{val.toBool()};
}

void Settings::setCachedKernelSupportsLRC(bool kernel_supports_lrc) {
  settings.setValue(SK_KernelSupportsLRC, kernel_supports_lrc);
}
