TEMPLATE = app
CONFIG += console
# sailfishapp is only needed for the appropriate INSTALLS target path
CONFIG += sailfishapp
CONFIG -= app_bundle
CONFIG -= qt

TARGET = harbour-battery-charging-control-cli

QMAKE_CXXFLAGS += -Wall -Wextra -Werror -Wmissing-include-dirs -Wredundant-decls -Woverloaded-virtual -Wundef -Wctor-dtor-privacy -Wcast-align -Wcast-qual -Wswitch-default -Wswitch-enum -Wfloat-equal -Wold-style-cast
#CONFIG += c++17 # doesn't work with current Qt
# TODO: change c++2a to c++20 with gcc 9
QMAKE_CXXFLAGS += -std=c++2a
# TODO can be removed with GCC 9:
LIBS += -lstdc++fs

SOURCES += cli.cpp \
  chargecontroller.cpp

HEADERS += \
  chargecontroller.hpp
