<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AutomaticMode</name>
    <message>
        <source>Manual mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upper Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current battery level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Automatic charging control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Charging will be automatically disabled if the battery level reaches the upper limit and enabled if it drops below this limit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This device doesn&apos;t support automatic charge control, so please keep the app running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can close the app now, your device will stop charging by itself until next reboot.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Battery voltage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Charge current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Charging...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discharging...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please report a bug: Your device is charging but no power supply seems to be connected!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discharging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>till limit:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Error</name>
    <message>
        <source>Something went wrong during startup.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManualMode</name>
    <message>
        <source>Automatic mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manual charging control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disable charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The external power source will continue to feed your device - only battery charging is stopped.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
